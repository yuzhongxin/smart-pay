package com.founder.service.impl;

import com.founder.core.dao.FianceAlRespository;
import com.founder.core.dao.FianceWxRespository;
import com.founder.core.domain.FianceAl;
import com.founder.core.domain.FianceWx;
import com.founder.core.log.MyLog;
import com.founder.service.IPayBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PayBillServiceImpl implements IPayBillService {

    private static final MyLog _log = MyLog.getLog(PayBillServiceImpl.class);

    @Autowired
    FianceAlRespository fianceAlRespository;

    @Autowired
    FianceWxRespository fianceWxRespository;

    @Override
    public int deleteWx(String mchId, String billDate) {
        return fianceWxRespository.deleteAllByMchidAndDate(mchId, billDate);
    }

    @Override
    public int deleteAli(String mchId, String billDate) {
        return fianceAlRespository.deleteAllByMchidAndDate(mchId, billDate);
    }

    @Override
    public List<FianceWx> selectWx(String mchId, String billDate, String keyName) {
        _log.info("按照关键字{}查询对账单");
        return fianceWxRespository.queryAllByMchidAndBzorder(mchId, billDate, keyName);
    }

    @Override
    public List<FianceAl> selectAli(String mchId, String billDate, String keyName) {
        _log.info("按照关键字{}查询对账单");
        return fianceAlRespository.queryAllByMchidAndBzorder(mchId, billDate, keyName);
    }

    @Override
    public List<FianceWx> saveWx(List<FianceWx> list) {
        return fianceWxRespository.saveAll(list);
    }

    @Override
    public List<FianceAl> saveAli(List<FianceAl> list) {
        return fianceAlRespository.saveAll(list);
    }
}
