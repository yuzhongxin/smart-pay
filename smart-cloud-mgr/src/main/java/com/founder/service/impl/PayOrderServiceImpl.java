package com.founder.service.impl;

import com.founder.core.dao.PayOrderRespository;
import com.founder.core.domain.PayOrder;
import com.founder.core.log.MyLog;
import com.founder.service.IPayOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import java.util.Date;
import java.util.List;

@Service
public class PayOrderServiceImpl implements IPayOrderService {

    private static final MyLog _log = MyLog.getLog(PayOrderServiceImpl.class);

    @Autowired
    PayOrderRespository payOrderRespository;

    @Override
    public PayOrder selectPayOrder(String payOrderId) {
        _log.info("按照订单号{}查询订单。", payOrderId);
        return payOrderRespository.getOne(payOrderId);
    }

    @Override
    public Page<PayOrder> selectPayOrderList(int offset, int limit, PayOrder payOrder) {
        _log.info("分页查询订单，offset={}，limit={}。", offset, limit);
        Pageable pageable = PageRequest.of(offset, limit);
        Page<PayOrder> page = payOrderRespository.findAll(new Specification<PayOrder>() {
            @Override
            public Predicate toPredicate(javax.persistence.criteria.Root<PayOrder> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Predicate predicate = criteriaBuilder.conjunction();
                if (payOrder != null){
                    String mchId = payOrder.getMchId();
                    if (StringUtils.isBlank(mchId)){
                        _log.info("传入商户号为空标识查询全部");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("mchId"), "%"));
                    } else {
                        _log.info("商户号模糊查询");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("mchId"), "%"+payOrder.getMchId().trim()+"%"));
                    }
                    String payOrderId = payOrder.getPayOrderId();
                    if (StringUtils.isBlank(payOrderId)){
                        _log.info("传入支付单号为空标识查询全部");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("payOrderId"), "%"));
                    } else {
                        _log.info("支付单号模糊查询");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("payOrderId"), "%"+payOrder.getPayOrderId().trim()+"%"));
                    }
                    if (payOrder.getStatus() != null && payOrder.getStatus() != -99) {
                        _log.info("订单状态-99标识全部");
                        predicate.getExpressions().add(criteriaBuilder.equal(root.get("status"), payOrder.getStatus()));
                    }
                }
                criteriaQuery.where(predicate);
                criteriaQuery.orderBy(criteriaBuilder.desc(root.get("createTime").as(Date.class)));

                return criteriaQuery.getRestriction();
            }
        }, pageable);
        return page;
    }

    @Override
    public List<PayOrder> getPayOrderList(int offset, int limit, PayOrder payOrder) {
        Pageable pageable = PageRequest.of(offset, limit);
        Example<PayOrder> example = Example.of(payOrder);
        Page<PayOrder> page = payOrderRespository.findAll(example, pageable);
        return page.getContent();
    }

    @Override
    public Integer count(PayOrder payOrder) {
        Example<PayOrder> example = Example.of(payOrder);
        Long count = payOrderRespository.count(example);
        return count.intValue();
    }
}
